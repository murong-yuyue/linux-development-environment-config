



# use tips
Ctrl + f

# paste line.
Shift + Ins

# copy line.
Ctrl + Ins

# fg bg jobs.
Ctrl + z

# break process.
Ctrl + c

# close one terminal.
Ctrl + d

# end of file.
Ctrl + d

# large terminal.
Ctrl + Shift + +
Ctrl + '+'

# small terminal.
Ctrl + '-'

# add one terminal.
Ctrl + Shift + t
Ctrl + 'T'

# open one terminal.
Ctrl + Alt + t


