
# other
	source config_file # config file action
    wc # total.

	ssh -p port user@host # ssh login.
	ssh-keygen # creat key.

# program build.
	make -j(number) # multi core build.

# command line use.
	info command
	man command
	whatis command
	whereis command
	which command


# system informantion.
	sudo -i # shift to root use current user password.
	uname # system core informantion.

	# watch -n 0.1的意思是以0.1秒的时间间隔刷新nvidia-smi的数据显示，显卡内存监控
	watch -n 0.1 nvidia-smi

	watch -n 0.1 free -h # free是内存显示命令，-h是以人类能读懂的格式显示

# network manage

    wget source_url direct_path # download file.

    # add network adapter's ip address.
	sudo ifconfig enp2s0:0 192.168.6.155 netmask 255.255.255.0 broadcast 192.168.6.255 up

# file manage
	mkdir postfix{1..2} # mkdir postfix1 postfix2 postfix3...

	tar -cvf file.tar file # zip
	tar -czf file.tar.gz file # zip

	tar -zxvf file.tar.gz file # unzip
	tar -xvf file.tar # unzip

    unzip source_zip -d direct_path # unzip
	unzip file.zip # unzip

